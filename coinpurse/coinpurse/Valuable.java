package coinpurse;

/**
 * interface for defining that every class or interface that implements this interface can get value.
 * @author Norawit Urailertprasert
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * get the value from the class.
	 * @return the value in double data type
	 */
	public double getValue();
}
