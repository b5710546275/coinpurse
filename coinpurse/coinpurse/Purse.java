package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import javax.lang.model.element.VariableElement;

import com.sun.org.apache.xpath.internal.operations.Variable;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A Valuable purse contains Valuables. You can insert Valuables, withdraw
 * money, check the balance, and check if the purse is full. When you withdraw
 * money, the Valuable purse decides which Valuables to remove.
 * 
 * @author Norwait Urailertprasert
 */
public class Purse extends Observable{
	List<Valuable> purse = new ArrayList<Valuable>();
	/**
	 * Capacity is maximum NUMBER of Valuables the purse can hold. Capacity is
	 * set when the purse is created.
	 */
	private int capacity;
	/**
	 * The object used to compare.
	 */
	private final ValueComparator comparator = new ValueComparator();
	/**
	 * A strategy that will be used to withdraw.
	 */
	WithdrawStrategy withdrawStrategy = new GreedyWithdraw();
	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity
	 *            is maximum number of Valuables you can put in purse.
	 */
	public Purse(int capacity) {
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of Valuables in the purse. This is the number
	 * of Valuables, not their value.
	 * 
	 * @return the number of Valuables in the purse
	 */
	public int count() {
		return purse.size();
	}

	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	public double getBalance() {
		double total = 0;
		for (Valuable i : purse)
			total += i.getValue();
		return total;
	}

	/**
	 * Return the capacity of the Valuable purse.
	 * 
	 * @return the capacity
	 */
	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 */
	public boolean isFull() {
		if (purse.size() == capacity)
			return true;
		return false;
	}

	/**
	 * Insert a Valuable into the purse. The Valuable is only inserted if the
	 * purse has space for it and the Valuable has positive value. No worthless
	 * Valuables!
	 * 
	 * @param valuable
	 *            is a Valuable object to insert into purse
	 * @return true if Valuable inserted, false if can't insert
	 */
	public boolean insert(Valuable valuable) {
		if (valuable == null)
			return false;
		if (valuable.getValue() < 0)
			return false;
		if (isFull())
			return false;
		purse.add(valuable);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**
	 * Withdraw the requested amount of money. Return an array of Valuables
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * @param amount
	 *            is the amount to withdraw
	 * @return array of Valuable objects for money withdrawn, or null if cannot
	 *         withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {
		Collections.sort(purse,new ValueComparator());
		Valuable[] withdrawed =withdrawStrategy.withdraw(amount, purse);
		if(withdrawed!=null){
			for (Valuable j : withdrawed) {
				for (int i = 0; i < purse.size(); i++) {
					if (purse.get(i).equals(j)) {
						purse.remove(i);
						break;
					}
				}
			}
		}
		return withdrawed;
	}
	/**
	 * This method used to set the withdraw strategy for the withdraw method.
	 * @param withdrawStrategy the strategy that will be used to withdraw.
	 */
	public void setWithdrawStrategy(WithdrawStrategy withdrawStrategy){
		this.withdrawStrategy = withdrawStrategy;
	}
	/**
	 * out put the information about the pure.
	 * 
	 * @return the max size, capacity, current size
	 */
	public String toString() {
		return "purse max size: " + capacity + "\n" + "purse size: "
				+ purse.size();
	}

}