package coinpurse;
import javax.sql.rowset.serial.SerialArray;

/**
 * A BankNote is a object that represent the bankNotein real life.
 * the note is used instead of the gold
 * to represent the value of some thing
 * @author Norawit Urailertprasert
 *
 */
public class BankNote extends AbstractValuable {
	/**
	 * store the value of the bank. initialize
	 */
	private double value;
	/**
	 * store a serial number of this bank.
	 */
	private String serialNumber;
	/**
	 * the serial counter.
	 */
	private static int serial =1;
	/**
	 * initialize the value of this bank note.
	 * @param value the value that wan to assign to the note
	 */
	public BankNote(double value, String currency){
		super(currency);
		this.value=value;
		serialNumber = String.format("%08d", serial);
		serial++;
	}
	/**
	 * test if the input have the same value as this object or not.
	 * @param obj the input that we want to test
	 * @return true if it have the same value
	 */
	public boolean equals(Object obj){
		if(obj==null)
			return false;
		if(obj.getClass()!=this.getClass())
			return false;
		Valuable other = (Valuable)obj;

		if(this.value==other.getValue())
			return true;
		return false;
	}
	/**
	 * get the value of the note in double.
	 * @return the value of the bank note
	 */
	public double getValue(){
		return value;
	}
	/**
	 * out put the vale of the bank note and the serial number.
	 * @return the value and the serial number
	 */
	public String toString(){
		return value+"-"+getCurrency()+" Banknote ["+serialNumber+"]";
	}

	
}
