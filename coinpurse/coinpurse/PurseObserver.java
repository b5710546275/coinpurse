package coinpurse;

import java.util.Observable;
import java.util.Observer;

/**
 * A Class that is use to observe the purse.
 * @author Norawit Urailertprasert
 *
 */
public class PurseObserver implements Observer{
	public PurseObserver(){
		
	}
	@Override
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse= (Purse)subject;
			double balance = purse.getBalance();
			//System.out.println("Balance is: "+balance);
		}
//		if(info!=null)
//			System.out.println(info);
		
	}

}
