package coinpurse.factory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;
/**
 * A factory class for creating a Thai Valuable.
 * @author Norawit Urailertprasert
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	private String currency = "Bath";
	private Double[] coin = new Double[]{1.0,2.0,5.0,10.0};
	private Double[] bankNote = new Double[]{20.0,50.0,100.0,500.0,1000.0};
	@Override
	public Valuable createMoney(double value){
		if (Arrays.asList(coin).contains(value)) return new Coin(value, currency);
		if (Arrays.asList(bankNote).contains(value)) return new BankNote(value,currency);
		throw new IllegalArgumentException();
		
	}
}
