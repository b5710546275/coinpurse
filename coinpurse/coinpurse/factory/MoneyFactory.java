package coinpurse.factory;

import java.util.ResourceBundle;

import coinpurse.Valuable;
/**
 * Abstract class for MoneyFactory.
 * @author Norawit Urailertprasert
 *
 */
public abstract class MoneyFactory {
	/**
	 * Protected constructor.
	 */
	protected MoneyFactory(){};
	/**
	 * Instance of this.
	 */
	private static MoneyFactory moneyFactory;
	/**
	 * Static class get a instance of the MoneyFactory.
	 * @return
	 */
	public static MoneyFactory getInstance(){
		if(moneyFactory==null){
			setMoneyFactory();
		}
		return moneyFactory;
	}
	/**
	 * Create a money base on what is set to be in that currency.
	 * @param value The value of the Valuable.
	 * @return A new Valuable object.
	 */
	public abstract Valuable createMoney(double value);
	/**
	 * Create a valuable from String.
	 * @param value a value of the Valuable
	 * @return A new Valuable object. 
	 */
	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}
	/**
	 * Set the instance of the MoneyFactory to what is set in a property file.
	 */
	public static void setMoneyFactory(){
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String factoryclass = bundle.getString("moneyfactory");
		System.out.println("Factory class is " + factoryclass); // testing
		try {
			moneyFactory = (MoneyFactory)Class.forName(factoryclass).newInstance();
		}
		catch(ClassNotFoundException e){
			System.err.print(e.getMessage());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
