package coinpurse.factory;

import java.util.Arrays;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;
/**
 * A Factory class for creating a Malaysian Valuable. 
 * @author Norawit Urailertprasert
 *
 */
public class MalaiMoneyFactory extends MoneyFactory{
	private String currency1 = "Sen";
	private String currency2 = "Ringgit";
	private Double[] coin = new Double[]{.05,.10,.20,.50};
	private Double[] bankNote = new Double[]{1.0,2.0,5.0,10.0,20.0,50.0,100.0};
	@Override
	public Valuable createMoney(double value) {
		if (Arrays.asList(coin).contains(value)) return new Coin(value*10, currency1);
		if (Arrays.asList(bankNote).contains(value)) return new BankNote(value,currency2);
		throw new IllegalArgumentException();
	}

}
