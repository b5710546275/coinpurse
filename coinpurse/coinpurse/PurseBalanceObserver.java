package coinpurse;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javafx.scene.text.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * A Observer for a balance of the purse.
 * it will display the balance of the purse.
 * @author Norawit Urailerprasert
 *
 */
public class PurseBalanceObserver extends JFrame implements Runnable,Observer{
	/**
	 * A label that will be display the balance.
	 */
	private JLabel balanceLabel = new JLabel();
	/**
	 * A constructor that will call init() to initialize.
	 */
	public PurseBalanceObserver() {
		init();
	}
	/**
	 * A initialize method that will initial all the component.
	 */
	public void init(){
		this.setPreferredSize(new Dimension(200,200));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(200, 200);
		balanceLabel.setText("0");
		balanceLabel.setHorizontalAlignment(0);
		balanceLabel.setFont(new java.awt.Font("helvetica", 0, 32 ));
		balanceLabel.setBackground(new Color(200,200,150));
		balanceLabel.setOpaque(true);
		this.add(balanceLabel);
	}
	@Override
	public void run() {
		setVisible(true);
		
	}
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof Purse){
			Purse purse = (Purse)o;
			balanceLabel.setText(String.valueOf(purse.getBalance())+" Baht");
			run();
		}
	}
	
	
}
