package coinpurse;

/**
 * Class that provide implementation for Valuable.
 * @author Norawit Urailertprasert
 *
 */
public abstract class AbstractValuable implements Valuable {
	/**
	 * A currency of this Valuable.
	 */
	private String currency;
	/**
	 * test if the input have the same color as this object or not.
	 * @param obj the object that we want to test
	 * @return true if same color
	 */
	public AbstractValuable(String currency) {
		this.currency=currency;
	}
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != this.getClass())
			return false;
		Valuable c = (Valuable) obj;
		if (c.getValue() == 0)
			return false;
		if (this.getValue() == c.getValue())
			return true;
		return false;
	}
	/**
	 * Compare between two Valuable.
	 * @param other the Valuable that will be compare
	 */
	@Override
	public int compareTo(Valuable other) {
		if(this.getValue()>other.getValue()){
			return 1;
		}
		if(this.getValue()<other.getValue()){
			return -1;
		}
		return 0;
	}
	
	public String getCurrency(){
		return currency;
	}
}
