package coinpurse;

import java.util.Comparator;

/**
 * class use to compare the the class that implement Valuable.
 * @author Norawit Urailertprasert
 */
public class ValueComparator implements Comparator<Valuable>{
/**
 * method use to compare the value between a and b.
 * @param a the first valuable
 * @param b the second valuble
 * @return a more than b return 1 
 *         b more than a return -1
 *         a equals b return 0
 */
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()>b.getValue())
			return 1;
		if(a.getValue()<b.getValue())
			return -1;
		return 0;
				
	}

}
