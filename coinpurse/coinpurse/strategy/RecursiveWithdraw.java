package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javafx.geometry.Side;
import javafx.scene.control.ListView;

import com.sun.org.apache.xpath.internal.operations.Variable;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * A recursive withdraw is a class that use to withdraw a valuable element from
 * a purse by using a recursive.
 * 
 * @author Norawit Urailertpraset
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuable) {
		List<Valuable> list = withdrawFrom(amount, valuable, 0);
		if(list!=null){
			Valuable[] output = new Valuable[list.size()];
			output = list.toArray(output);
			return output;
		}
		return null;
	}
	/**
	 * A method that help used recursive to withdraw.
	 * @param amount the amount of the Valuable needed/
	 * @param list the list of the Valuable.
	 * @param lastIndex the index with will be use to get the element.
	 * @return the list the element that is added up to be the answer or a null.
	 */
	private List<Valuable> withdrawFrom(double amount, List<Valuable> list,int lastIndex) {	
		
		if (amount == 0) {
			return new ArrayList<Valuable>();
		}
		if (amount < 0 ||list==null|| (amount > 0 && list.size() == 0)) {
			return null;
		}
		if(lastIndex>=list.size()){
			return null;
		}
		List<Valuable> newList = withdrawFrom(amount- list.get(lastIndex).getValue(),list.subList(1, list.size()), lastIndex);
		if(newList!=null){
			newList.add(list.get(lastIndex));
			return newList;
		}
		else{
			lastIndex++;
			if(lastIndex>=list.size()){
				return null;
			}
			List<Valuable> newsList =withdrawFrom(amount, list, lastIndex);
			return newsList;
		}
	}
}
