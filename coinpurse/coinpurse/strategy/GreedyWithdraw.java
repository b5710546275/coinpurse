package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * A withdraw strategy that using a greedy algrolithim.
 * @author Norawit Urailertprasert
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		Collections.sort(valuables);
		System.out.println(Arrays.toString(valuables.toArray()));
		if (amount > 0) {
			ArrayList<Valuable> temp = new ArrayList<Valuable>();
			// Collections.sort(purse);
			for (int i = valuables.size()-1; i >=0; i--) {
				if (amount >= valuables.get(i).getValue()) {
					temp.add(valuables.get(i));
					amount -= valuables.get(i).getValue();
					
				}
			}
			if (amount == 0) {
				Valuable[] outPurse = new Valuable[temp.size()];
				outPurse = temp.toArray(outPurse);
				return outPurse;
			}
			return null;
		}
		return null;
	}

}
