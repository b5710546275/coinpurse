package coinpurse.strategy;
import java.util.List;

import coinpurse.Valuable;

/**
 * A strategy interface use for defining the strategy to use with withdrawing from coin.
 * @author Norawit Urailertprasert
 *
 */
public interface WithdrawStrategy {
	/**
	 * A method used to telling what should the purse withdraw.
	 * @param amount the amount of money that wanted to be withdraw.
	 * @param valuable the list of a Valuable item in the purse.
	 * @return the suggestion from the algorithm.
	 */
	Valuable[] withdraw(double amount,List<Valuable> valuable);
}
