
package coinpurse;

import coinpurse.factory.MoneyFactory;
import coinpurse.strategy.RecursiveWithdraw;


/**
 * A main class to create objects and connect objects together. The user
 * interface needs a reference to coin purse.
 * 
 * @author Norawit Urailertprasert
 */
public class Main {

	/**
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		Purse purse = new Purse(10);
		purse.addObserver(new PurseObserver());
		purse.addObserver(new PurseBalanceObserver());
		purse.addObserver(new PurseStatusObserver());
		purse.setWithdrawStrategy(new RecursiveWithdraw());
		ConsoleDialog consoleDialog = new ConsoleDialog(purse);
		consoleDialog.run();
//		MoneyFactory factory = MoneyFactory.getInstance();
//		Valuable m = factory.createMoney( 1.0 );
//		System.out.println(m.toString());

	}
}