package coinpurse;

import java.util.Arrays;
import java.util.List;

/**
 * test the purse.
 * 
 * @author Norwait Urailertprasert
 *
 */
public class Test {
	/**
	 * main method.
	 * 
	 * @param args
	 *            the input
	 */
	public static void main(String[] args) {
		Purse purse = new Purse(15);
//		Coin coin1 = new Coin(2);
//		Coin coin2 = new Coin(2);
//		Coin coin3 = new Coin(5);
//		Coin coin4 = new Coin(2);
//		System.out.println(purse.insert(coin1));
//		System.out.println(purse.insert(coin2));
//		System.out.println(purse.insert(coin3));
//		System.out.println(purse.insert(coin4));
//
//		System.out.println(Arrays.toString(purse.withdraw(6)));

		ConsoleDialog console = new ConsoleDialog(purse);
		console.run();
		
//		BankNote b1 = new BankNote(200);
//		BankNote b2 = new BankNote(50);
//		Coupon c1 = new Coupon("red");
//		Coupon c = new Coupon("blue");
//		System.out.println(purse.insert(coin1));
//		System.out.println(purse.insert(c));
//		System.out.println(purse.insert(b1));
//		System.out.println(purse.insert(b2));
//		System.out.println(purse.insert(c1));
//		System.out.println(purse.toString());
//		System.out.println(purse.getBalance());
//		System.out.println(purse.count());
		
//		Valuable[] stuff = purse.withdraw(410);
//		System.out.println(Arrays.toString(stuff));
//		System.out.println(stuff[0].toString());
//		System.out.println(stuff[1].toString());
//		System.out.println(Arrays.toString(stuff));
//		System.out.println(purse.getBalance());
		
//		System.out.println("a".compareTo("b"));
//		List<String> food = Arrays.asList("apple","b","c","d");
//		ListUtil.printList(food);
	}
}
