package coinpurse;

import java.util.HashMap;
import java.util.Map;



/**
 * Coupan is a paper that represent the value.
 * this is used in KU fair
 * @author Norawit Urailertprasert
 
 */
public class Coupon extends AbstractValuable{
	/**
	 * store the the color of the coupon.
	 */
	private String color;
	/**
	 * initialize the value and color of the coupon.
	 * @param color of the coupon
	 */
	public Coupon(String color,String currency){
		super(currency);
		this.color=color.toLowerCase();
	}
	/**
	 * get the value of the coupon.
	 * @return value of the coupon
	 */
	public double getValue(){
		Map<String,Double> map = new HashMap<String, Double>();
		map.put("red", new Double(100));
		map.put("blue", new Double(50));
		map.put("green", new Double(20));
		return map.get(color).doubleValue();
	}

	/**
	 * output to String telling which color does the coupon is.
	 * @return String "color coupon"
	 */
	public String toString(){
		Map<String,String> map = new HashMap<String, String>();
		map.put("red","Red");
		map.put("blue", "Blue");
		map.put("green","Green");
		return map.get(color)+" coupon";
	}
	
}
