package coinpurse;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import sun.net.www.content.image.jpeg;

import com.sun.xml.internal.ws.api.server.Container;
/**
 * A Observer for a status.
 * It will the percent of the number in the purse.
 * If it full it will tell that it full.
 * @author Norawit Urailertprasert
 *
 */
public class PurseStatusObserver extends JFrame implements Runnable,Observer {
	/**
	 * The progress bar that will tell the percent of the number in the purse.
	 */
	private JProgressBar progress = new JProgressBar();
	/**
	 * The label that will display true when it full.
	 */
	private JLabel label;
	/**
	 * A constructor that will call init().
	 */
	public PurseStatusObserver() {
		super("Purse Status");
		init();
	}
	/**
	 * A initialize method for initial all the needed compronent.
	 */
	public void init(){
		JLabel outerLabel = new JLabel();
		outerLabel.setLayout(new BoxLayout(outerLabel, BoxLayout.Y_AXIS));
		setSize(200,200);
		setLocation(0, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		progress.setMaximum(100);
		progress.setMinimum(0);
		progress.setValue(10);
		progress.setStringPainted(true);
		progress.setSize(200,100);
		label = new JLabel("EMPTY");
		label.setSize(100,50);
		label.setHorizontalAlignment(0);
		outerLabel.add(label);
		outerLabel.add(progress);
		add(outerLabel);
	}
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof Purse){
			Purse purse =(Purse) o;
			if(!purse.isFull()){
				progress.setVisible(true);
				progress.setValue((int) (((double)purse.count())/purse.getCapacity()*100));
				label.setText("number of items: "+purse.count());
			}
			else {
				progress.setValue((int) (((double)purse.count())/purse.getCapacity()*100));
				progress.setForeground(new Color(0,255,0));
				progress.setOpaque(true);
				label.setText("FULL");
			}		
		}
		run();
	}
	@Override
	public void run() {
		setVisible(true);
	}
}
