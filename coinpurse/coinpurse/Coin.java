package coinpurse;

/**
 * A coin with a monetary value. You can't change the value of a coin.
 * 
 * @author Norawit Urailertprasert
 */
public class Coin extends AbstractValuable {
	private String currency;
	/** Value of the coin .*/
	private double value;

	/**
	 * Constructor for a new coin.
	 * @param value
	 *            is the value for the coin
	 */
	public Coin(double value, String currency) {
		super(currency);
		if (value > 0) {
			this.value = value;
		}
		else{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Method for get value for the coin.
	 * @return value of the coin
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @Override Method to test that the coin have equal value or not
	 * @param obj that we want to test
	 * @return true if it equal false if it not equal
	 */
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != this.getClass())
			return false;
		Coin c = (Coin) obj;
		if (c.getValue() == 0)
			return false;
		if (this.value == c.value)
			return true;
		return false;
	}



	/**
	 * output to value in String form.
	 * @return the value of the coin in Bath
	 */
	public String toString() {
		return value + "-"+getCurrency()+" coin";
	}

}
