package coinpurse;
import java.util.Arrays;
import java.util.List;
/**
 * A utility class use for a purse.
 * 
 * @author Norawit Urailertprasert
 *
 */
public class ListUtil {
	/**
	 * A method that will print all the element in the list by using recursion of another method(printEachElement).
	 * @param list 
	 */
	public static void printList(List<String> list){
		if(list.size() >1){
			System.out.print(list.get(0)+",");
			printList((List<String>) (list.subList(1, list.size())));
		}
		System.out.println(list.get(0));
		
	}	
	private static String max(List<String> list){
		if(list.size()==1){
			return list.get(0);
		}
		if(list.get(0).compareTo(list.get(1))>0){
			return max((List<String>) list.subList(1, list.size()));
		}
		else{
			list.set(0, list.get(1));
			return max((List<String>) list.subList(1, list.size()));
		}
	}
	/**
	 * main method used to start a program.
	 * @param args a input
	 */
	public static void main(String[] args){
		List<String> list;
		if(args.length>0) list = Arrays.asList(args);
		else list = Arrays.asList("b","z","c","p","zz");
		
		System.out.println("List contains ");
		System.out.println(Arrays.toString(list.toArray()));
		printList(list);
		System.out.println();
		
		String max = max(list);
		System.out.println("Lexical greatest element is "+max);
	}
}
